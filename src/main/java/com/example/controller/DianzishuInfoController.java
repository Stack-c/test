package com.example.controller;

import com.example.common.Result;
import com.example.entity.Account;
import com.example.entity.DianzishuInfo;
import com.example.service.DianzishuInfoService;
import com.example.vo.DianzishuInfoVo;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/dianzishuInfo")
public class DianzishuInfoController {

    @Resource
    private NxSystemFileController nxSystemFileController;
    @Resource
    private DianzishuInfoService dianzishuInfoService;
    @PostMapping
    public Result<DianzishuInfo> add(@RequestBody DianzishuInfo info, HttpServletRequest request) {
        Account account = (Account) request.getSession().getAttribute("user");
        info.setUserName(account.getName());
        info.setLevel(account.getLevel());
        info.setUploadUserId(account.getId());
        dianzishuInfoService.add(info);
        return Result.success(info);
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Long id, HttpServletRequest request) {
        Account account = (Account) request.getSession().getAttribute("user");
        DianzishuInfo info = dianzishuInfoService.findById(id);
        if (!account.getLevel().equals(info.getLevel()) || !account.getId().equals(info.getUploadUserId())) {
            return Result.error("1001", "不能删除他人的记录");
        }
        dianzishuInfoService.delete(id);
        // 删除对应文件记录
        if (info.getFileId() != null) {
            nxSystemFileController.deleteFile(info.getFileId().toString());
        }
        return Result.success();
    }

    @PutMapping
    public Result update(@RequestBody DianzishuInfo info, HttpServletRequest request) {
        Account account = (Account) request.getSession().getAttribute("user");
        if (!account.getLevel().equals(info.getLevel()) || !account.getId().equals(info.getUploadUserId())) {
            return Result.error("1001", "不能修改他人的记录");
        }
        dianzishuInfoService.update(info);
        return Result.success();
    }

    @GetMapping("/{id}")
    public Result<DianzishuInfoVo> detail(@PathVariable Long id) {
        DianzishuInfoVo info = dianzishuInfoService.findById(id);
        return Result.success(info);
    }

    @GetMapping
    public Result<List<DianzishuInfoVo>> all() {
        return Result.success(dianzishuInfoService.findAll());
    }

    @GetMapping("/page/{name}")
    public Result<PageInfo<DianzishuInfoVo>> page(@PathVariable String name,
                                             @RequestParam(defaultValue = "1") Integer pageNum,
                                             @RequestParam(defaultValue = "5") Integer pageSize,
                                             HttpServletRequest request) {
        return Result.success(dianzishuInfoService.findPage(name, pageNum, pageSize));
    }

}
