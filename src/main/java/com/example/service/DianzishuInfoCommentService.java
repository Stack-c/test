package com.example.service;

import com.example.exception.CustomException;
import com.example.dao.DianzishuInfoCommentDao;
import org.springframework.stereotype.Service;
import com.example.entity.DianzishuInfoComment;
import com.example.vo.DianzishuInfoCommentVo;
import com.example.entity.Account;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class DianzishuInfoCommentService {

    @Resource
    private DianzishuInfoCommentDao dianzishuInfoCommentDao;

    public DianzishuInfoComment add(DianzishuInfoComment commentInfo, HttpServletRequest request) {
        Account user = (Account) request.getSession().getAttribute("user");
        if (user == null) {
            throw new CustomException("1001", "请先登录！");
        }
        commentInfo.setName(user.getName());
        commentInfo.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        dianzishuInfoCommentDao.insertSelective(commentInfo);
        return commentInfo;
    }

    public void delete(Long id) {
        dianzishuInfoCommentDao.deleteByPrimaryKey(id);
    }

    public void update(DianzishuInfoComment commentInfo) {
        commentInfo.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        dianzishuInfoCommentDao.updateByPrimaryKeySelective(commentInfo);
    }

    public DianzishuInfoComment findById(Long id) {
        return dianzishuInfoCommentDao.selectByPrimaryKey(id);
    }

    public List<DianzishuInfoCommentVo> findAll() {
        return dianzishuInfoCommentDao.findAllVo(null);
    }

    public PageInfo<DianzishuInfoCommentVo> findPage(String name, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<DianzishuInfoCommentVo> all = dianzishuInfoCommentDao.findAllVo(name);
        return PageInfo.of(all);
    }

    public List<DianzishuInfoCommentVo> findByForeignId (Long id) {
        List<DianzishuInfoCommentVo> all = dianzishuInfoCommentDao.findByForeignId(id, 0L);
        for (DianzishuInfoCommentVo reserveInfoVo : all) {
            Long parentId = reserveInfoVo.getId();
            List<DianzishuInfoCommentVo> children = new ArrayList<>(dianzishuInfoCommentDao.findByForeignId(id, parentId));
            reserveInfoVo.setChildren(children);
        }
        return all;
    }
}
