package com.example.service;

import cn.hutool.core.collection.CollectionUtil;
import com.example.dao.DianzishuInfoDao;
import com.example.entity.DianzishuInfo;
import com.example.vo.DianzishuInfoVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DianzishuInfoService {

    @Resource
    private DianzishuInfoDao dianzishuInfoDao;

    public DianzishuInfo add(DianzishuInfo info) {
        dianzishuInfoDao.insertSelective(info);
        return info;
    }

    public void delete(Long id) {
        dianzishuInfoDao.deleteByPrimaryKey(id);
    }

    public void update(DianzishuInfo info) {
        dianzishuInfoDao.updateByPrimaryKeySelective(info);
    }

    public DianzishuInfoVo findById(Long id) {
        List<DianzishuInfoVo> list = dianzishuInfoDao.findByNameAndId(null, id);
        if (!CollectionUtil.isEmpty(list)) {
            return list.get(0);
        }
        return new DianzishuInfoVo();
    }

    public List<DianzishuInfoVo> findAll() {
        return dianzishuInfoDao.findByNameAndId("all", null);
    }

    public PageInfo<DianzishuInfoVo> findPage(String name, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<DianzishuInfoVo> info = dianzishuInfoDao.findByNameAndId(name, null);
        return PageInfo.of(info);
    }
}
